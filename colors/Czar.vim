" Vim color file
" Converted from Textmate theme Czar using Coloration v0.4.0 (http://github.com/sickill/coloration)

set background=dark
highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "Czar"

hi Cursor ctermfg=234 ctermbg=188 cterm=NONE
hi Visual ctermfg=NONE ctermbg=238 cterm=NONE
hi CursorLine ctermfg=NONE ctermbg=236 cterm=NONE
hi CursorColumn ctermfg=NONE ctermbg=236 cterm=NONE
hi ColorColumn ctermfg=NONE ctermbg=236 cterm=NONE
hi LineNr ctermfg=214 ctermbg=236 cterm=NONE
hi VertSplit ctermfg=240 ctermbg=240 cterm=NONE
hi MatchParen ctermfg=39 ctermbg=NONE cterm=underline
hi StatusLine ctermfg=231 ctermbg=240 cterm=bold
hi StatusLineNC ctermfg=231 ctermbg=240 cterm=NONE
hi Pmenu ctermfg=230 ctermbg=NONE cterm=NONE
hi PmenuSel ctermfg=NONE ctermbg=238 cterm=NONE
hi IncSearch ctermfg=234 ctermbg=196 cterm=NONE
hi Search ctermfg=NONE ctermbg=NONE cterm=underline
hi Directory ctermfg=39 ctermbg=NONE cterm=NONE
hi Folded ctermfg=71 ctermbg=234 cterm=NONE

hi Normal ctermfg=231 ctermbg=234 cterm=NONE
hi Boolean ctermfg=39 ctermbg=NONE cterm=NONE
hi Character ctermfg=39 ctermbg=NONE cterm=NONE
hi Comment ctermfg=71 ctermbg=NONE cterm=NONE
hi Conditional ctermfg=39 ctermbg=NONE cterm=NONE
hi Constant ctermfg=39 ctermbg=NONE cterm=NONE
hi Define ctermfg=39 ctermbg=NONE cterm=NONE
hi DiffAdd ctermfg=231 ctermbg=64 cterm=bold
hi DiffDelete ctermfg=88 ctermbg=NONE cterm=NONE
hi DiffChange ctermfg=231 ctermbg=23 cterm=NONE
hi DiffText ctermfg=231 ctermbg=24 cterm=bold
hi ErrorMsg ctermfg=231 ctermbg=124 cterm=NONE
hi WarningMsg ctermfg=231 ctermbg=124 cterm=NONE
hi Float ctermfg=214 ctermbg=NONE cterm=NONE
hi Function ctermfg=230 ctermbg=NONE cterm=NONE
hi Identifier ctermfg=39 ctermbg=NONE cterm=NONE
hi Keyword ctermfg=39 ctermbg=NONE cterm=NONE
hi Label ctermfg=196 ctermbg=NONE cterm=NONE
hi NonText ctermfg=240 ctermbg=None cterm=NONE
hi Number ctermfg=214 ctermbg=NONE cterm=NONE
hi Operator ctermfg=39 ctermbg=NONE cterm=NONE
hi PreProc ctermfg=39 ctermbg=NONE cterm=NONE
hi Special ctermfg=231 ctermbg=NONE cterm=NONE
hi SpecialKey ctermfg=240 ctermbg=NONE cterm=NONE
hi Statement ctermfg=39 ctermbg=NONE cterm=NONE
hi StorageClass ctermfg=39 ctermbg=NONE cterm=NONE
hi String ctermfg=196 ctermbg=NONE cterm=NONE
hi Tag ctermfg=230 ctermbg=NONE cterm=NONE
hi Title ctermfg=231 ctermbg=NONE cterm=bold
hi Todo ctermfg=71 ctermbg=NONE cterm=inverse,bold
hi Type ctermfg=230 ctermbg=NONE cterm=NONE
hi Underlined ctermfg=NONE ctermbg=NONE cterm=underline
hi htmlTag ctermfg=39 ctermbg=NONE cterm=NONE
hi htmlEndTag ctermfg=39 ctermbg=NONE cterm=NONE
hi htmlTagName ctermfg=39 ctermbg=NONE cterm=NONE
hi htmlArg ctermfg=39 ctermbg=NONE cterm=NONE
hi htmlSpecialChar ctermfg=221 ctermbg=NONE cterm=NONE
hi yamlKey ctermfg=230 ctermbg=NONE cterm=NONE
hi yamlAnchor ctermfg=39 ctermbg=NONE cterm=NONE
hi yamlAlias ctermfg=39 ctermbg=NONE cterm=NONE
hi yamlDocumentHeader ctermfg=196 ctermbg=NONE cterm=NONE
hi cssURL ctermfg=39 ctermbg=NONE cterm=NONE
hi cssFunctionName ctermfg=230 ctermbg=NONE cterm=NONE
hi cssColor ctermfg=39 ctermbg=NONE cterm=NONE
hi cssPseudoClassId ctermfg=230 ctermbg=NONE cterm=NONE
hi cssClassName ctermfg=230 ctermbg=NONE cterm=NONE
hi cssValueLength ctermfg=214 ctermbg=NONE cterm=NONE
hi cssCommonAttr ctermfg=230 ctermbg=NONE cterm=NONE
hi cssBraces ctermfg=NONE ctermbg=NONE cterm=NONE
